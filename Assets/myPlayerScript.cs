
using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;
public class myPlayerScript : MonoBehaviourPun
{
    PhotonView _pv;
   
    private void Awake()
    {
        _pv = gameObject.GetComponent<PhotonView>(); 
    }
   
  

    
    

    private void OnTriggerStay(Collider other)    // if player stays then enable camera and allow player to write on board
    {
      
        if (other.gameObject.CompareTag("BoardLogic"))
        {//

            if (_pv.IsMine)
            {
              //  if (!other.gameObject.GetComponent<draw>().isCollided)
                {
                    // _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All);
                    _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All, other.gameObject.tag.ToString());
                   // other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(true); 
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(true);

                    if (other.gameObject.GetComponent<draw>().IsOccupied == false)
                    {
                        other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(true);
                        other.gameObject.GetComponent<draw>().SpectateBoardbutton.gameObject.SetActive(false);
                        other.gameObject.GetComponent<draw>().editWithOtherButton.gameObject.SetActive(false);
                    }

                    else if (other.gameObject.GetComponent<draw>().boardButton.gameObject.transform.GetChild(0).GetComponent<Text>().text != "Exit Board")
                    {
                        other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(false);
                        other.gameObject.GetComponent<draw>().SpectateBoardbutton.gameObject.SetActive(true);
                        other.gameObject.GetComponent<draw>().editWithOtherButton.gameObject.SetActive(true);
                    }


                    
                }

            }
        }
        else if (other.gameObject.CompareTag("BoardLogic1"))
        {//

            if (_pv.IsMine)
            {
             //   if (!other.gameObject.GetComponent<draw>().isCollided)
                {
                    // _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All);
                    _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All, other.gameObject.tag.ToString());
                  //  other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(true);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(true);

                    if (other.gameObject.GetComponent<draw>().IsOccupied == false)
                    {
                        other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(true);
                        other.gameObject.GetComponent<draw>().SpectateBoardbutton.gameObject.SetActive(false);
                        other.gameObject.GetComponent<draw>().editWithOtherButton.gameObject.SetActive(false);
                    }

                    else if (other.gameObject.GetComponent<draw>().boardButton.gameObject.transform.GetChild(0).GetComponent<Text>().text != "Exit Board")
                    {
                        other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(false);
                        other.gameObject.GetComponent<draw>().SpectateBoardbutton.gameObject.SetActive(true);
                        other.gameObject.GetComponent<draw>().editWithOtherButton.gameObject.SetActive(true);
                    }
                }

            }
        }
        else if (other.gameObject.CompareTag("BoardLogic2"))
        {//

            if (_pv.IsMine)
            {
             //   if (!other.gameObject.GetComponent<draw>().isCollided)
                {
                    // _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All);
                    _pv.RPC("SetLineRenderBoardTrue", RpcTarget.All, other.gameObject.tag.ToString());
                 //   other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(true);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(true);

                    if (other.gameObject.GetComponent<draw>().IsOccupied == false)
                    {
                        other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(true);
                        other.gameObject.GetComponent<draw>().SpectateBoardbutton.gameObject.SetActive(false);
                        other.gameObject.GetComponent<draw>().editWithOtherButton.gameObject.SetActive(false);
                    }

                    else if (other.gameObject.GetComponent<draw>().boardButton.gameObject.transform.GetChild(0).GetComponent<Text>().text != "Exit Board")
                    {
                        other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(false);
                        other.gameObject.GetComponent<draw>().SpectateBoardbutton.gameObject.SetActive(true);
                        other.gameObject.GetComponent<draw>().editWithOtherButton.gameObject.SetActive(true);
                    }
                }

            }
        }


    }



    private void OnTriggerExit(Collider other)
    {
    
        if (other.gameObject.CompareTag("BoardLogic"))  // detect collision nd proseed as per board 
        {
            if (_pv.IsMine)
            {
              //  if (other.gameObject.GetComponent<draw>().isCollided )
                {
                   
                    _pv.RPC("SetLineRenderBoardFalse", RpcTarget.All, other.gameObject.tag.ToString()); //, "and jup.");
                    other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(false);

                    other.gameObject.GetComponent<draw>().SpectateBoardbutton.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().editWithOtherButton.gameObject.SetActive(false);
                }
            }
        }
        else if (other.gameObject.CompareTag("BoardLogic1"))
        {
            if (_pv.IsMine)
            {
             //   if (other.gameObject.GetComponent<draw>().isCollided )
                {
                    //      _pv.RPC("SetLineRenderBoardFalse", RpcTarget.All);
                    _pv.RPC("SetLineRenderBoardFalse", RpcTarget.All, other.gameObject.tag.ToString()); //, "and jup.");
                    other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(false);

                    other.gameObject.GetComponent<draw>().SpectateBoardbutton.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().editWithOtherButton.gameObject.SetActive(false);
                }
            }
        }
        else if (other.gameObject.CompareTag("BoardLogic2"))
        {
            if (_pv.IsMine)
            {
             //   if (other.gameObject.GetComponent<draw>().isCollided )
                {
                   
                    _pv.RPC("SetLineRenderBoardFalse", RpcTarget.All, other.gameObject.tag.ToString()); //, "and jup.");
                    other.gameObject.GetComponent<draw>().boardButton.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().deleteAllBtn.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().SpectateBoardbutton.gameObject.SetActive(false);
                    other.gameObject.GetComponent<draw>().editWithOtherButton.gameObject.SetActive(false);
                }
            }
        }


    }

    [PunRPC]
    void SetLineRenderBoardTrue(string tagval)
    {
   
        GameObject.FindGameObjectWithTag(tagval).GetComponent<draw>().isCollided=true;
       // GameObject.FindGameObjectWithTag(tagval).GetComponent<draw>().IsOccupied = true;

    }

    [PunRPC]
    void SetLineRenderBoardFalse(string tagval)
    {
        GameObject.FindGameObjectWithTag(tagval).GetComponent<draw>().isCollided = false;
       // GameObject.FindGameObjectWithTag(tagval).GetComponent<draw>().IsOccupied = false;
    }
}


