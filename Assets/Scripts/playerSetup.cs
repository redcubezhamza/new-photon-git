using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using SUPERCharacter;
using UnityEngine.UI;
using Photon.Realtime;
using agora_gaming_rtc;

public class playerSetup : MonoBehaviourPunCallbacks
{
    public PhotonView pView;
    public playerName pName;
    public Text NameTxt, NameTxtBack;
    public GameObject MultiChanel;


    private string ChanelNameCheck;



    // public GameObject ShareScreenBtn;







    // Start is called before the first frame update
    private void Awake()
    {
        if (pView.IsMine)
        {
            MultiChanel.SetActive(true);
        }





    }
    void Start()
    {

        // ShareScreenBtn.SetActive(false);
        pView = GetComponent<PhotonView>();

        //stop assigning controls if this is not the player related to this peer
        if (pView.IsMine)
        {
            ChanelNameCheck = "lobby";

            Debug.Log("mYDATA");
            GetComponent<SUPERCharacterAIO>().enabled = true;
            GetComponent<SUPERCharacterAIO>().enableMovementControl = true;
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            pView.RPC("PlayerName", RpcTarget.All, PhotonManager.playername.username);

            //ShareScreenBtn.SetActive(true);

        }
        else
        {
            GetComponent<SUPERCharacterAIO>().enabled = false;
            GetComponent<SUPERCharacterAIO>().enableMovementControl = false;
            gameObject.transform.GetChild(0).gameObject.SetActive(false);
            //ShareScreenBtn.SetActive(false);
        }





        /* foreach (Player p in PhotonNetwork.PlayerList)
             {

                 if (p.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
                 {

                     ShareScreenBtn.SetActive(false);

                 }
                 else
                 {

                     ShareScreenBtn.SetActive(true);
                 }


             }*/



    }
    [PunRPC]
    public void PlayerName(string playername)
    {

        pName = new playerName(playername);
        // transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = pView.Owner.NickName;
        NameTxt.text = pName.username;
        NameTxtBack.text = pName.username;

    }
    // Update is called once per frame

    void OnTriggerEnter(Collider col)
    {
        if (pView.IsMine)
        {
            if (col.tag == "cafe" && ChanelNameCheck != "cafe")
            {
                ChanelNameCheck = "cafe";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().channelName = "cafa";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().Button_LeaveChannel();
                MultiChanel.GetComponent<MultiChannelSceneCtrl>().EnableVideothe(false);


            }
            else if (col.tag == "lobby" && ChanelNameCheck != "lobby")
            {
                ChanelNameCheck = "lobby";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().channelName = "lobby";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().Button_LeaveChannel();
                MultiChanel.GetComponent<MultiChannelSceneCtrl>().EnableVideothe(false);
            }
            else if (col.tag == "waiting" && ChanelNameCheck != "waiting")
            {
                ChanelNameCheck = "waiting";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().channelName = "waiting";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().Button_LeaveChannel();
                MultiChanel.GetComponent<MultiChannelSceneCtrl>().EnableVideothe(false);
            }
            else if (col.tag == "meeting" && ChanelNameCheck != "meeting")
            {
                ChanelNameCheck = "meeting";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().channelName = "meeting";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().Button_LeaveChannel();
                MultiChanel.GetComponent<MultiChannelSceneCtrl>().EnableVideothe(true);
            }
            else if (col.tag == "cafe" && ChanelNameCheck != "cafe")
            {
                ChanelNameCheck = "cafe";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().channelName = "cafa";
                MultiChanel.transform.GetChild(0).transform.GetComponent<AgoraChannelPanel>().Button_LeaveChannel();
                MultiChanel.GetComponent<MultiChannelSceneCtrl>().EnableVideothe(false);

            }
        }
    }

}
