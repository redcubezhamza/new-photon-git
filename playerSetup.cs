using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using SUPERCharacter;
public class playerSetup : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        if (photonView.IsMine)
        {
            GetComponent<SUPERCharacterAIO>().enabled = true;
        }
        else
        {
            GetComponent<SUPERCharacterAIO>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
